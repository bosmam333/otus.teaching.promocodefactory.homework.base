﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<EmployeeShortResponse> CreateEmployee([FromBody] EmployeeShortRequest requestModel)
        {
            var employeeModel = new Employee()
            {
                Id = Guid.NewGuid(),
                Email = requestModel.Email,
                FirstName = requestModel.FirstName,
                LastName = requestModel.LastName
            };

            await _employeeRepository.CreateAsync(employeeModel);

            var responseModel = new EmployeeShortResponse()
            {
                Id = employeeModel.Id,
                Email = employeeModel.Email,
                FullName = employeeModel.FullName
            };

            return responseModel;
        }

        /// <summary>
        /// Обновить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEmployee([FromBody] EmployeeRequest requestModel)
        {
            var employeeModel = new Employee()
            {
                Id = requestModel.Id,
                Email = requestModel.Email,
                Roles = requestModel.Roles?.Select(x => new Role()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FirstName = requestModel.FirstName,
                LastName = requestModel.LastName,
                AppliedPromocodesCount = requestModel.AppliedPromocodesCount
            };

            var existedEmployee = await _employeeRepository.GetByIdAsync(employeeModel.Id);

            if(existedEmployee == null)
            {
                await _employeeRepository.CreateAsync(employeeModel);
                return StatusCode(201);
            }

            await _employeeRepository.UpdateAsync(employeeModel);
            return StatusCode(200);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployee(Guid id)
        {
            if(await _employeeRepository.DeleteAsync(id))
                return StatusCode(204);

            return NotFound();
        }
    }
}