﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data.ToList();
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task CreateAsync(T item)
        {
            Data.Add(item);

            return Task.CompletedTask;
        }

        public Task UpdateAsync(T item)
        {
            DeleteAsync(item.Id);
            CreateAsync(item);

            return Task.CompletedTask;
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            var item = GetByIdAsync(id).GetAwaiter().GetResult();

            if(item != null)
            {
                Data.Remove(item);
                return Task.FromResult(true);
            }

            return Task.FromResult(false);
        }
    }
}